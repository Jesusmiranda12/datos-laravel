@extends('layouts.app')

@section('content')
<div class="container">
<form action="{{route('datos.store')}}" method="POST">
@csrf
    <div class="row justify-content-center">
        <div class="col-md-8">
            <h1>DATOS PERSONALES</h1>        
            <input type="text" placeholder="Nombre" name="nombre" class="form-control"> <br>
            <input type="text" placeholder="Apellido paterno" name="apellidop" class="form-control"> <br>
            <input type="text" placeholder="Apellido materno" name="apellidom" class="form-control"> <br>
            <input type="date" placeholder="Fecha de nacimiento" name="fecha" class="form-control"> <br>
            <input type="submit" value="Guardar" class="btn btn-primary">        
        </div>
    </div>
</form>
</div>
@endsection