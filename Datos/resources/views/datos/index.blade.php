@extends('layouts.app')

@section('content')
<div class="text-center"><h1>Datos</h1></div>
<div class="container">
<table class="table table-bordered">
<thead class="thead-dark">
<tr>
<th>ID</th>
<th>Nombre</th>
<th>Apellido paterno</th>
<th>Apellido materno</th>
<th>Fecha de nacimiento</th>
<th>Acciones</th>
</tr>
</thead>
<tbody>
@foreach($datos as $dato)
<tr>
<td>{{$dato->id}}</td>
<td>{{$dato->nombre}}</td>
<td>{{$dato->apellidop}}</td>
<td>{{$dato->apellidom}}</td>
<td>{{$dato->fecha}}</td>
<td>Accion</td>
</tr>
@endforeach
</tbody>
</table>
<form action="{{route('datos.create')}}" method="GET">
<input type="submit" value="Nuevo" class="btn btn-success">
</form>
</div>
@endsection